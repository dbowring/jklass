BaseClass = jKlass.klass({
    $meta: {
        class_level: {
            classname: function() {
                return 'Base';
            }
        },
        getters: [jKlass.attr('a')],
        setters: [jKlass.attr('b')],
        accessors: [jKlass.attr('c')]
    },
    name: function() {
        return 'BaseClass';
    }
});

BaseClassClone = jKlass.klass({
    $meta: {
        //clone_def: true, // Here's the difference
        class_level: {
            classname: function() { return 'BaseClone';}
        },
        getters: [jKlass.attr('a')],
        setters: [jKlass.attr('b')],
        accessors: [jKlass.attr('c')]
    },
    name: function() {
        return 'BaseClassClone';
    }
});

ChildClass1 = jKlass.klass({
    $meta: {
        inherits: [BaseClass],
        class_level: {
            classname: function() {return this.apply_super(ChildClass1, 'classname') + '.Child';},
            class_value: 42
        }
    },
    name: function() {
        return this.apply_super(ChildClass1, 'name', arguments) + '.ChildClass1';
    }
});

ChildClass2 = jKlass.klass({
    $meta: {
        inherits: [ChildClass1],
        class_level: {
            classname: function() {return this.apply_super(ChildClass2, 'classname') + '.GrandChild';}
        }
    },
    name: function() {
        return this.apply_super(ChildClass2, 'name', arguments) + '.ChildClass2';
    }
});


Animal = jKlass.klass({
    // This is the classes definition. It contains the meta data and any
    // instance methods to be defined
    $meta: {
        getters: [jKlass.attr('name')], // We will be able to get this attribute
        setters: [jKlass.attr('nickname')], // We will be able to set this one
        accessors: [jKlass.attr('can_fly')], // We will be able to get and set this!

        class_level: {
            // Class methods can also be defined, and are called with
            // the class as the "this" value.
            special_powers: function() {
                return this.has_magic ? 'magic' : 'none';
            },
            has_magic: true
        }

    },

    // We can define instance methods by including them here
    // Everything EXCEPT the meta object will be interpreted to be a
    // instance method
    toString: function() {
        return this.nickname() + 's species is ' + this.constructor.species();
    }
});


function get_table() {
    return document.getElementById('results_table');
}

function get_time() {
    return (new Date()).getTime();
}

function test(description, test_fn, expectation) {
    var result;
    var start = get_time();
    try {
        result = test_fn();
    } catch (err) {
        result = err;
    }
    var delta = get_time()-start;
    var passed = result == expectation;
    var rows = [description, expectation, result, delta];
    var tr = document.createElement('tr');
    var td = null;

    for (var i=0; i<rows.length; i++) {
        td = document.createElement('td');
        td.innerText = rows[i];
        tr.appendChild(td);
    }
    if (passed) {
        tr.setAttribute('class', 'passed');
    } else {
        tr.setAttribute('class', 'failed');
    }
    get_table().appendChild(tr);
}

function t(x){ return new TestClass(x); }

window.onload = function() {
    var a = new BaseClass({
        a: 'a value',
        b: 'b value',
        c: 'c value'
    }), b = new ChildClass1({
        a: '1a value',
        b: '1b value',
        c: '1c value'
    }), c = new ChildClass2({
        a: '2a value',
        b: '2b value',
        c: '2c value'
    }), d = new ChildClass2(c.__data__);

    test('Instance Methods', function() {
        return a.name();
    }, 'BaseClass');
    test('Instance Methods Using Super', function() {
        return b.name();
    }, 'BaseClass.ChildClass1');
    test('Instance Methods Using Deep Super', function() {
        return c.name();
    }, 'BaseClass.ChildClass1.ChildClass2');

    test('Class Methods', function() {
        return BaseClass.classname();
    }, 'Base');
    test('Class Methods Using Super', function() {
        return ChildClass2.classname();
    }, 'Base.Child.GrandChild');

    test('Class Variables #1', function() {
        return BaseClass.class_value;
    }, undefined);
    test('Class Variables #2', function() {
        return ChildClass1.class_value;
    }, 42);
    test('Class Variables #3', function() {
        return ChildClass2.class_value;
    }, 42);

    test('Attr Reader', function() {
        return a.a();
    }, 'a value');
    test('Attr Writer', function() {
        a.b('new b value');
        return a.__data__.b;
    }, 'new b value');
    test('Attr Accessor', function() {
        a.c('new c value');
        return a.c();
    }, 'new c value');

    test('Attr Reader', function() {
        return b.a();
    }, '1a value');
    test('Attr Writer', function() {
        b.b('new 1b value');
        return b.__data__.b;
    }, 'new 1b value');
    test('Attr Accessor', function() {
        b.c('1new c value');
        return b.c();
    }, '1new c value');

    test('Attr Reader', function() {
        return c.a();
    }, '2a value');
    test('Attr Writer', function() {
        c.b('2new b value');
        return c.__data__.b;
    }, '2new b value');
    test('Attr Accessor', function() {
        c.c('2new c value');
        return c.c();
    }, '2new c value');

    test('Attr Reader Unchanged', function() {
        return a.a();
    }, 'a value');



    window.a = a = new Animal({
        name: 'Daniel',
        nickname: 'DanielB',
        can_fly: false
    });

    test('Sample Check #1', function() {
        return a.name();
    }, 'Daniel');

    test('Sample Check #2', function() {
        a.nickname('DanB');
        return a.__data__.nickname;
    }, 'DanB');

    test('Sample Check #3', function() {
        return a.can_fly();
    }, false);

    test('Sample Check #3', function() {
        a.can_fly(true);
        return a.can_fly();
    }, true);

    test('Sample Check #4', function() {
        return Animal.has_magic;
    }, true);

    test('Sample Check #5', function() {
        return Animal.special_powers();
    }, 'magic');

    test('Definition Check', function() {
        // data and x.__data__ should be the same object
        var data = { c: 1 };
        var x = new BaseClass(data);
        x.c(2);
        return data.c == x.c();
    }, true);
};

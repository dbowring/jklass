jKlass 3.x
============

(c) 2012 Daniel Bowring.

jKlass is a Class, Object and Inheritance implementation for JavaScript. 
Features include super methods for both instances and classes, code generation
(getters, setters and accessors), JSON serialization, multiple inheritance, and
more.

### Detailed notes are available in the source code!

This is just a brief overview, I will provide a full example package soon

Usage
-----

```js
    myClass = jKlass.klass(argument_object)
```

Valid argument object keys:

* `$meta`
    * Define special properties for the object. Accepts:
        * `class_level`
            * object to apply as class level variables/methods
        * `inherits`
            * Array of objects to inherit from
        * `getters`
            * Array of readers to create. For each key, create a method:
                * `instance.key()` -> `instance.__def__[key]`
            
        * `setters`
            * Array of writers to create. For each key, create a method:
                * `instance.key(value)` -> `instance.__def__[key] = value`
        * `accessors`
            * reader/writer combo
        * `$new`
            * Modifies the way creating new instances is performed
            * Returns an array to be treated as the "new" arguments to the
                constructor
        * `json`
            * Allows for defining exactly how to serialize to JSON, if you do
                not wish the used the built-in method.
            * subkeys
                * `class_name` name of this class. required if you're 
                    going to use JSON
                * `class_key` key to save the class name under. defaults
                    to `json_class`
                * `asJSON`
                    * function that returns a JSON representable object
                        * that is, an array or hash-like object containing
                            only numbers, strings, booleans or other arrays
                            and hash-like objects (or other objects that
                            support toJSON)
                    * defaults to built-in method (returns __data__ object)
                * `fromJSON`
                    * Function that takes the json object and returns a class
                        instance
                    * defaults to built-in method (which should work for any
                        object using the standard `$new` method)

        * `generate`
            * Used to generate instance and class methods
            * subkeys
                * `instance_methods`
                    * syntax
                        * `[[[method_name, [args, to, pass]], fn], ..]
                * `class_methods`
                    * same syntax as instance_methods
* `$init`
    * Called when a new object is created, with any given arguments (except
        the first, which is assumed to be the argument_object)
* Anything else
    * All other keys will be treated as instance variables/methods

Inheritance / Super
-------------------

```js
Person = jKlass.klass({
    $meta: {
        getters: [jKlass.attr('name')]
    },
    add_title: function() {
        return this.name();
    }
});

Doctor = jKlass.klass({
    $meta: {
        inherits: [Person]
    },
    add_title: function() {
        return 'Dr. ' + this.name();
    }
});

Mysterious = jKlass.klass({
    $meta: {
        inherits: [Person]
    },
    add_title: function() {
        return '???';
    }
});

NotSoMysterious = jKlass.klass({
    $meta: {
        inherits: [Mysterious]
    },
    add_title: function() {
        return this.apply_super(NotSoMysterious, 'add_title') + '(' + this.name() + ')';
    }
});

Person2 = jKlass.klass({
    $meta: {
        $new: function(name) {
            return [{name: name}];
        },
        inherits: [Person]
    }
});

Person3 = jKlass.klass({
    $meta: {
        $new: function(name) {
            return [{name: name}, Array.prototype.slice.call(arguments, 1)];
        },
        inherits: [Person]
    },
    $init: function(greet) {
        if (greet) {
            alert('Hello from ' + this.name() + '!');
        }
    }
});

var foo = new Person({name: 'Foo'});
var bar = new Doctor({name: 'Bob'});
var baz = new Mysterious({name: 'Dr. Who'});
var qux = new NotSoMysterious({name: 'Jim'});

foo.add_title(); // -> "Foo"
bar.add_title(); // -> "Dr. Foo"
baz.add_title(); // -> "???"
qux.add_title(); // -> "???(Fred)"

var foobar = new Person2('Tim');
var barbaz = new Person2('Rob', false);

```

You can also use this.apply_super at the class level and it will work properly

JSON support
------------

* jKlass objects are designed to for seamless use with JSON serialization.
* To make the most of this, separate your "persistant" data from everything
    else
    * That is, if you would send it with JSON, it should be a part of __data__,
        everything else is a normal attribute on the instance
    * Although you can limit (or add) what data is sent by setting an asJSON
        method


Basic JSON usage


```js

myClass = jKlass.klass({
    $meta: {
        readers: [jKlass.attr("myvar")],
        json_class: "myClass"
    }
});

var myInstance = new myClass({"myvar": "Some Data"});
var dump = JSON.stringify([myInstance]);
var loaded = JSON.parse(dump, jKlass.json_revive([myClass]));
console.log(loaded[0].myvar()) // -> "Some Data";

```

$new method
-----------

Using the `$meta.$new` option, you can change how to initialize an object

```js

Person = jKlass.klass({
    $meta: {
        $new: function(name) {
            // returns the __data__ object. gets whatever arguments are used
            // in the new statement
            return {n: name};
        },
        getter: [jKlass.attr('name', ['n'])]
    }
});

var daniel = new Person('Daniel');
daniel.name()  // -> 'Daniel'
```

Note that changing the $new method will require you to define a new fromJSON
method (using $meta.json.fromJSON) if you wish JSON support to be continued

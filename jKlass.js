
jKlass = (function(){

    function toJSON() {
        // This method is used the JSON.stringify to convert the object into
        // something serializable. jKlass provides the functionality here to
        // ensure that it will be able to restore it later

        // The asJSON method can be used by the developer to modify the
        // output to add or remove fields
        var og = this.asJSON();
        // We're going to have to make a copy of the object so that this
        // function doesn't have any side-effects
        var copy = {};

        // Shallow copy
        for (var k in og) {
            copy[k] = og[k];
        }

        // Here we add the class key (default: json_class) so that it can
        // later be known what to revive it as
        var c = this.constructor;
        copy[c.$json_class_key] = c.$json_class_name;

        // Give the copy to JSON to stringify
        return copy;
    }

    function default_asJSON() {
        // Default method of converting jKlass object into a JSON serialization
        // In this instance, the __data__ object is given (as is its intended
        // use)
        // This method does not return a JSON string, but instead returns
        // an object that can be represented in JSON (that is only arrays,
        //  hash-like objects, strings and simple types [numeric, boolean])
        return this.__data__;
    }

    function default_fromJSON(data) {
        // The default method of converting from a JSON object into a given
        // jKlassObject. this refers the the class.
        return new this(data);
    }

    function instance_super(cls, name) {
        for (var i=0; i<cls.$supers.length; i++) {
            if (cls.$supers[i].prototype[name] !== undefined) {
                return cls.$supers[i].prototype[name];
            }
        }
    }
    function class_super(cls, name) {
        for (var i=0; i<cls.$supers.length; i++) {
            if (cls.$supers[i][name] !== undefined) {
                return cls.$supers[i][name];
            }
        }
    }

    function apply_instance_super(source, name) {
        return instance_super(source, name).apply(
            // this, Array.prototype.slice.call(arguments, 2)
            this, jKlass.slice(arguments, 2)
        );
    }

    function apply_class_super(source, name) {
        return class_super(source, name).apply(
            // this, Array.prototype.slice.call(arguments, 2)
            this, jKlass.slice(arguments, 2)
        );
    }

    var jKlass = {
        version: '3.1.4',
        jKlassObject: function() {
            // Everytime a new class/object-type is needed, we need to generate
            // a new function to act as the base. The standard constructor is
            // also contained here
            return function jKlassObject(data) {
                // If a $new method is available, use it to change the data
                if (this.constructor.$new) {
                    arguments = this.constructor.$new.apply(
                        this.constructor, arguments
                    );
                }

                // Store the data object. This is the core of our new object
                this.__data__ = arguments[0] || {};

                // Check for an $init function
                if (this.$init) {
                    // Pass on any superfluous arguments to $init
                    this.$init.apply(
                        // this, Array.prototype.slice.call(arguments, 1)
                        this, jKlass.slice(arguments, 1)
                    );
                }
            };
        },
        slice: function(obj) {
            return Array.prototype.slice.apply(
                arguments[0],
                Array.prototype.slice.call(
                    arguments,
                    1
                )
            );
        },
        klass: function(definition) {
            // This is what you'll be interacting with the most
            // Generate a new class from the given definition
            if (arguments.length != 1) {
                throw "jKlass.klass takes exactly one argument";
            }

            var base = this.jKlassObject();

            for (var k in definition) {
                // Copy from the definition into the prototype, ignoring any
                // keys starting with $ (except for $init). This ensures
                // that the $meta definition won't be included, and you
                // wont accidentally interfere with internals (which use the
                //  dollar-sign prefix)
                if (k[0] != '$' || k == '$init') {
                    base.prototype[k] = definition[k];
                }
            }

            if (definition.$meta) {
                this.apply_meta(base, definition.$meta);
            }

            return base;
        },
        apply_meta: function(base, meta) {
            // Apply the meta information to the new class "base"
            // All standard $meta keys are stored in the meta_methods
            // attribute, with the value being a function that implements
            // them. The function takes two arguments: the base function and
            // the value in the $meta definition
            // This makes dispatching the implementations very easy
            for (var k in meta) {
                if (this.meta_methods[k]) {
                    this.meta_methods[k](base, meta[k]);
                } else {
                    // I'm still deciding what to do here. I may throw an error
                    // or implement something to allow for custom meta keys
                    // throw "Unknown meta option " + k;
                }
            }
        },
        meta_methods: {
            $new: function(base, fn) {
                // the $new function is a bit tricky, because it changes the
                // way some things work. $new should take some arguments
                // and return an Array to act as the "new" arguments to $init.
                // The object can then be created without having to use the
                // hash-like syntax, which can be cumbersome in certain cases
                // for example, assuming $new on the class Person is:
                /*
                     function(name) {
                        // Note the return type must be an array
                        return [{name: name}];
                    }
                */
                // to create a new Person you use this code:
                /* new Person(name); */
                // As usual, any extra values will be given to $init (that is,
                // __data__ = arguments[0],$init.apply(this,arguments.slice(1))
                // This means, however, you will have to implement a fromJSON
                // method if you want to be able to use jKlass.json_revive
                base.$new = fn;
            },
            inherits: function(base, from) {
                // Inherit instance-level (from the prototype) variables
                // I'm still deciding whether to include class-level variables
                // in this, which means you could do
                // this.constructor.class_var instead of MyClass.class_var,
                // but the value across the classes would be inconsistent as
                // each class would have its own copy
                // (think @@var in ruby)
                var k;
                for (var i=0; i<from.length; i++) {
                    for (k in from[i].prototype) {
                        if (base.prototype[k] === undefined) {
                            base.prototype[k] = from[i].prototype[k];
                        }
                    }
                    for (k in from[i]) {
                        if (base[k] === undefined) {
                            base[k] = from[i][k];
                        }
                    }
                }
                base.apply_super = apply_class_super;
                base.prototype.apply_super = apply_instance_super;
                // We'll need this information for the super method
                base.$supers = from;
            },
            _make_getter: function(key) {
                // Generate and return a getter function
                return function getter() {
                    return this.__data__[key];
                };
            },
            _make_setter: function(key) {
                // Generate and return a setter function
                return function setter(value) {
                    this.__data__[key] = value;
                    return this;  // allow for chaining
                };
            },
            _make_accessor: function(key) {
                // Generate and return a accessor function
                // These work the same as jQuery methods like val, text, ...
                // in that if you give any arguments, its a setter, otherwise
                // it is a getter
                var getter = this._make_getter(key);
                var setter = this._make_setter(key);
                return function accessor(value) {
                    if (arguments.length > 0) {
                        return setter.call(this, value);
                    } else {
                        return getter.call(this);
                    }
                };
            },
            _generate_access: function(base, attributes, generator) {
                // Deals with get/set/access aliasing syntax
                var i, j, fn;
                for (i=0; i < attributes.length; i++) {
                    fn = generator.call(this, attributes[i].source);
                    for (j=0; j < attributes[i].names.length; j++) {
                        base.prototype[attributes[i].names[j]] = fn;
                    }
                }
            },
            getters: function(base, attributes) {
                // getters are functions that return a value
                this._generate_access(base, attributes, this._make_getter);
            },
            setters: function(base, attributes) {
                // setters are function that change a value. In jKlass, they
                // return the instance which they operated on. This is to allow
                // for method chaining
                this._generate_access(base, attributes, this._make_setter);
            },
            accessors: function(base, attributes) {
                // accessors are a get/set combination (see _make_accessor)
                this._generate_access(base, attributes, this._make_accessor);
            },
            class_level: function(base, o) {
                // Copy key-value pairs onto the class (function)
                for (var k in o) {
                    base[k] = o[k];
                }
            },
            json: function(base, options) {
                // JSON compatibility
                if (!options.class_name) {
                    throw "Must provide a class_name";
                }
                // asJSON converts an object into a JSON representable object
                base.prototype.asJSON = options.asJSON || default_asJSON;
                base.prototype.toJSON = toJSON;
                base.fromJSON = options.fromJSON || default_fromJSON;
                // Unique identifier (preferably Globally unique) so jKlass
                // can identify the target class when parsing the JSON
                base.$json_class_name = options.class_name;
                // Name of the key to store the class name in
                base.$json_class_key = options.class_key;
            },
            _generate_methods: function(target, source) {
                // Generate methods using source and place them on target
                var methods, fn;
                for (var i=0; i<source.length; i++) {
                    methods = source[i][0];
                    fn = source[i][1];
                    for (var j=0; j<methods.length; j++) {
                        target[methods[j][0]] = fn(methods[j][1]);
                    }
                }
            },
            generate: function(base, options) {
                // Allows of instance and class methods (or values, if needed)
                // to be generated from a name, arguments and generation
                // function
                // syntax is as follows
                /*
                    // ...
                    generate: {
                        instance_methods: [
                            [
                                ['method_name', ['generator', 'args'],
                                function(arg1, arg2) {
                                    // in this case arg1='generator' and
                                    // arg2='args'
                                    return function() {
                                        // ...
                                    }
                                }
                            ],
                            // ...
                        ]
                    }
                    // ...
                */
                if (options.instance_methods) {
                    this._generate_methods(
                        base.prototype, options.instance_methods
                    );
                }
                if (options.class_methods) {
                    this._generate_methods(
                        base, options.instance_methods
                    );
                }
            }
        },
        json_revive: function(targets) {
            // Generate a JSON reviver function for JSON.parse
            // targets is an Array of objects that the data may revive into
            // for example
            /*
                Animal = jKlass.klass({ ... });
                Person = jKlass.klass({ ... });
                // ...
                r = JSON.parse(json_data, jKlass.json_revive([Animal, Person]);
                console.log(r)  // -> [Person, Person, Animal] // or similar
            */
            return function(key, value) {
                var c, k;
                for (var i=0; i<targets.length; i++) {
                    c = targets[i];
                    k = c.$json_class_key;
                    if (k) {
                        if (value && value[k] == c.$json_class_name) {
                            delete value[k];
                            return c.fromJSON(value);
                        }
                    }
                }
                return value;
            };
        },
        attr: function(source, names) {
            // This is sugar for the getter/setter/accesor methods
            if (names === undefined) {
                names = [source];
            } else if (typeof(names) == "string") {
                names = [names];
            }
            return {source: source, names: names};
        }
    };

    // Linters will complain about the following line
    jKlassModule = new (jKlass.klass.call(jKlass, jKlass));
    
    return jKlassModule;
})();
